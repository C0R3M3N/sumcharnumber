﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class BigNum
    {

        public static String Sum(String n1, String n2)
        {
            String kq = "";
            if (n1 == null) n1 = "0";
            if (n2 == null) n2 = "0";
            int maxlength = Math.Max(n1.Length, n2.Length);
            int m = 0; //biến nhớ

            char c1;   //c1 là ký tự tại vị trí i1 của chuỗi n1
            char c2;   //c2 là ký tự tại vị trí i2 của chuỗi n2

            int i1 = 0;     //i1 là vị trí ký tự cần lấy của n1 tại bước lặp thứ i 
            int i2 = 0;     //i2 là vị trí ký tự cần lấy của n2 tại bước lặp thứ i

            int d1 = 0;// giá trị interger của c1
            int d2 = 0; // giá trị interger của c2

            for (int i = 0; i < maxlength; i++)
            {
                
                i1 = n1.Length - 1 - i;
                if (i1 >= 0)
                {
                    c1 = n1[i1];
                    d1 = c1 - '0';
                }

                i2 = n2.Length - 1 - i;
                if (i2 >= 0)
                {
                    c2 = n2[i2];
                    d2 = c2 - '0';
                }
                
                int rs = d1 + d2 + m;
                d1 = 0; d2 = 0; m = 0; //reset value for new loop
                if (rs >= 10)
                {
                    rs = rs - 10;
                    m = 1;
                }
                kq = kq.Insert(0, rs.ToString());
            }

            if (m == 1)
            {
                kq = kq.Insert(0, "1");
            }
            return kq;
        }
    }
}
