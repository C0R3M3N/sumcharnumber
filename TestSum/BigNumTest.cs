using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;

namespace TestSum
{
    [TestClass]
    public class BigNumTest
    {
        [TestMethod]
        public void Sum_ResultTest()
        {
            //arrange
            string a = "123456";
            string b = "87654";

            //Act
            var result = BigNum.Sum(a, b);

            //Assert
            string expect = "211110";
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Sum_A_greater_Than_B()
        {
            //arrange
            string a = "123456378263876682498273987957";
            string b = "87654";

            //Act
            var result = BigNum.Sum(a, b);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Sum_B_greater_Than_A()
        {
            //arrange
            string a = "999";
            string b = "2984738472049582498345823565667";

            //Act
            var result = BigNum.Sum(a, b);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Sum_with_Null()
        {
            //arrange
            string a = "45345";
            string b = null;

            //Act
            var result = BigNum.Sum(a, b);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Sum_with_both_Null()
        {
            //arrange
            string a = null;
            string b = null;

            //Act
            var result = BigNum.Sum(a, b);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Sum_with_emty_string()
        {
            //arrange
            string a = "";
            string b = "1245";

            //Act
            var result = BigNum.Sum(a, b);

            //Assert
            Assert.IsNotNull(result);
        }
    }
}
